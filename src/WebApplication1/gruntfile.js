﻿module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ts');

    grunt.initConfig({
        ts: {
            base: {
                src: ['ClientApp/app/boot.ts', 'ClientApp/app/**/*.ts'],
                outDir: 'wwwroot/app',
                tsconfig: './tsconfig.json'
            }
        },
        
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'ClientApp/style',
                    src: ['*.scss'],
                    dest: 'wwwroot/style',
                    ext: '.css'
                }]
            }
        },

        uglify: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'wwwroot/app',
                    src: ['**/*.js'],
                    dest: 'wwwroot/app'
                }]
            },
            options: {
                sourceMap: true
            }
        },

        // Copy all JS files from external libraries and required NPM packages to wwwroot/js
        copy: {
            main: {
                files: [{
                        expand: true,
                        flatten: true,
                        src: [
                            'ClientApp/js/**/*.js',
                            'node_modules/es6-shim/es6-shim.min.js',
                            'node_modules/systemjs/dist/system-polyfills.js',
                            'node_modules/angular2/bundles/angular2-polyfills.js',
                            'node_modules/systemjs/dist/system.src.js',
                            'node_modules/rxjs/bundles/Rx.js',
                            'node_modules/angular2/bundles/angular2.dev.js'
                        ],
                        dest: 'wwwroot/js/',
                        filter: 'isFile'
                    }
                    /*{ expand: true, cwd: 'ClientApp/fonts/', src: ['**'], dest: 'wwwroot/fonts' },
                    { expand: true, cwd: 'ClientApp/images/', src: ['**'], dest: 'wwwroot/images' },
                    { expand: true, cwd: 'ClientApp/scripts/', src: ['**'], dest: 'wwwroot/scripts' }*/
                ]
            }
        },

        // Watch specified files and define what to do upon file changes
        watch: {
            scripts: {
                files: ['ClientApp/**/*.js'],
                tasks: ['ts', 'uglify', 'copy']
            }
        }
    });

    // Define the default task so it will launch all other tasks
    grunt.registerTask('default', ['ts', 'sass', 'uglify', 'copy']);

    // Define the default task so it will launch all other tasks
    grunt.registerTask('withWatch', ['default', 'watch']);
};